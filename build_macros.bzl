def my_cc_binary(name, srcs, copts = [], linkopts = [], deps = []):
    native.cc_binary(
        name = name,
        srcs = srcs,
        copts = copts + [
            "-I/usr/local/include",
            "-g",
            "-O3",
            "-std=c++20",
            "-Wall",
        ],
        linkopts = linkopts + [
            "-lpthread",
        ],
        deps = deps + [
            "//src:includes",
            "//third_party/fmt",
            "//third_party/spdlog",
            "//third_party/tomlplusplus",
            "//third_party/cxxopts",
        ],
    )
