#include <iostream>
#include <string>
#include <spdlog/spdlog.h>
#include <fmt/format.h>
#include <fmt/core.h>
#include <toml++/toml.h>

int main(int argc, char** argv) {
    std::string a = fmt::format("good {}!", "morning");
    std::cout << a << std::endl;

    spdlog::info("hello world");

    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <TOML file path>" << std::endl;
        return 1;
    }

    const char* file_path = argv[1];

    try {
        toml::table tbl = toml::parse_file(file_path);
        std::cout << "TOML file parsed successfully!" << std::endl;

        // Print the parsed TOML content
        std::cout << tbl << std::endl;
    } catch (const toml::parse_error& err) {
        std::cerr << "Error parsing TOML file: " << err.what() << std::endl;
        return 1;
    }

    return 0;
}